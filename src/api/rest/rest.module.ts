import { Module } from '@nestjs/common';
import { ParticipantsModule } from 'src/api/rest/participants/participants.module';
import { CommonModule } from './common/common.module';

@Module({
  imports: [ParticipantsModule, CommonModule],
})
export class RestApiModule {}
