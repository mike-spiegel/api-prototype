import { InjectMapper } from '@automapper/nestjs';
import { Mapper } from '@automapper/types';
import { Body, Controller, Get, Param, Post, Query } from '@nestjs/common';
import { ApiQuery } from '@nestjs/swagger';
import { Builder } from 'builder-pattern';
import { Participants } from 'src/core/dataaccess/entities/participants.entity';
import { CreateParticipantDto } from './dto/create-participant.dto';
import { ListParticipantResponse } from './dto/list-participants.dto';
import { ReadParticipantDto } from './dto/read-participant.dto';
import { ParticipantsService } from './participants.service';

@Controller('participants')
export class ParticipantsController {
  constructor(
    private readonly participantsService: ParticipantsService,
    @InjectMapper() private mapper: Mapper,
  ) {}

  @Post()
  create(@Body() createParticipantDto: CreateParticipantDto) {
    return this.participantsService.create(createParticipantDto);
  }

  @Get()
  @ApiQuery({ name: 'createdAfter', required: false, type: Date })
  @ApiQuery({ name: 'modifiedAfter', required: false, type: Date })
  @ApiQuery({ name: 'createdOrModifiedAfter', required: false, type: Date })
  async findAll(
    @Query('createdAfter') createdAfter?: Date,
    @Query('modifiedAfter') modifiedAfter?: Date,
    @Query('createdOrModifiedAfter') createdOrModifiedAfter?: Date,
  ): Promise<ListParticipantResponse> {
    const entities = await this.participantsService.findAll();
    const results = entities.map((p) =>
      this.mapper.map(p, ReadParticipantDto, Participants),
    );
    return Builder<ListParticipantResponse>().results(results).build();
  }

  @Get(':id')
  @ApiQuery({ name: 'createdAfter', required: false, type: Date })
  @ApiQuery({ name: 'modifiedAfter', required: false, type: Date })
  @ApiQuery({ name: 'createdOrModifiedAfter', required: false, type: Date })
  findOne(
    @Param('id') id: string,
    @Query('createdAfter') createdAfter?: Date,
    @Query('modifiedAfter') modifiedAfter?: Date,
    @Query('createdOrModifiedAfter') createdOrModifiedAfter?: Date,
  ) {
    return this.participantsService.findOne(+id);
  }
}
