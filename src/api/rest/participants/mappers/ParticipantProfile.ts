import { mapFrom } from '@automapper/core';
import { AutomapperProfile, InjectMapper } from '@automapper/nestjs';
import { Mapper } from '@automapper/types';
import { Injectable } from '@nestjs/common';
import { ReadParticipantDto } from 'src/api/rest/participants/dto/read-participant.dto';
import { Participants } from '../../../../core/dataaccess/entities/participants.entity';

@Injectable()
export class ParticipantProfile extends AutomapperProfile {
  constructor(@InjectMapper() mapper: Mapper) {
    super(mapper);
  }
  mapProfile() {
    return (mapper: Mapper) => {
      mapper.createMap(Participants, ReadParticipantDto).forMember(
        (destination) => destination.createdAt,
        mapFrom((source) => source.insertedAt),
      );
    };
  }
}
