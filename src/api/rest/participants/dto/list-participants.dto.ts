import { ListResponseDto } from '../../common/dto/list-response.dto';
import { ReadParticipantDto } from './read-participant.dto';

export class ListParticipantResponse
  implements ListResponseDto<ReadParticipantDto>
{
  results: ReadParticipantDto[];
}
