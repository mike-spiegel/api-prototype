// import { CustomVariableValues } from '../entities/customVariableValues';
// import { ParticipantContact } from '../entities/participantContact';
// import { ParticipantStudyInfo } from '../entities/participantStudyInfo';

import { AutoMap } from '@automapper/classes';
import { IsDateString, IsString } from 'class-validator';

export class ReadParticipantDto {
  @IsString()
  @AutoMap()
  readonly globalId: string;

  @IsString()
  @AutoMap()
  readonly firstName?: string;

  @IsString()
  @AutoMap()
  readonly lastName?: string;

  @IsString()
  @AutoMap()
  readonly middleName?: string;

  @IsString()
  @AutoMap()
  readonly birthday?: string;

  @IsString()
  @AutoMap()
  readonly race?: string;

  @IsString()
  @AutoMap()
  readonly sex?: string;

  @IsString()
  @AutoMap()
  readonly ethnicity?: string;

  @IsDateString()
  readonly createdAt?: Date;

  @IsDateString()
  @AutoMap()
  readonly modifiedAt?: Date;
  //   readonly studies?: Array<ParticipantStudyInfo>;
  //   readonly customVariables?: Array<CustomVariableValues>;
  //   readonly contacts?: Array<ParticipantContact>;
}
