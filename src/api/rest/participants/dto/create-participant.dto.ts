import { OmitType } from '@nestjs/swagger';
import { ReadParticipantDto } from './read-participant.dto';

export class CreateParticipantDto extends OmitType(ReadParticipantDto, [
  'createdAt',
  'modifiedAt',
] as const) {}
