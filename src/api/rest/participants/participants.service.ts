import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Participants } from 'src/core/dataaccess/entities/participants.entity';
import { Repository } from 'typeorm';
import { CreateParticipantDto } from './dto/create-participant.dto';
import { ReadParticipantDto } from './dto/read-participant.dto';
import { UpdateParticipantDto } from './dto/update-participant.dto';

@Injectable()
export class ParticipantsService {
  constructor(
    @InjectRepository(Participants)
    private participantRepository: Repository<Participants>,
  ) {}
  create(createParticipantDto: CreateParticipantDto) {
    return new CreateParticipantDto();
  }

  // FIXME: unhardcode limit
  async findAll(limit = 100): Promise<Participants[]> {
    return this.participantRepository.find({
      take: limit,
    });
  }

  findOne(id: number): ReadParticipantDto {
    return new ReadParticipantDto();
  }

  update(id: number, updateParticipantDto: UpdateParticipantDto) {
    return `This action updates a #${id} participant`;
  }

  remove(id: number) {
    return `This action removes a #${id} participant`;
  }
}
