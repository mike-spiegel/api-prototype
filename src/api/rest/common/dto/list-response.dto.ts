export interface ListResponseDto<T> {
  // additional collection-specific metadata should go here, i.e. pagination links, sort etc.
  // see https://github.com/microsoft/api-guidelines/blob/vNext/Guidelines.md#9-collections
  // and https://github.com/WhiteHouse/api-standards#record-limits
  // for common cases
  readonly results: T[];
}
