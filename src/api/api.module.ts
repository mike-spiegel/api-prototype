import { Module } from '@nestjs/common';
import { RestApiModule } from './rest/rest.module';

@Module({
  imports: [RestApiModule],
})
export class ApiModule {}
