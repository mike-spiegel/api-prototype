import { NestFactory } from '@nestjs/core';
import { DocumentBuilder, SwaggerModule } from '@nestjs/swagger';
import { AppModule } from './app.module';

async function bootstrap() {
  const app = await NestFactory.create(AppModule);

  const swaggerConfig = new DocumentBuilder()
    .setTitle('Ripple Science API')
    .setDescription(getApiDescription())
    .setVersion('1.0')
    .addSecurity('hmac', {
      type: 'http',
      scheme: 'hmac',
    })
    .build();

  const document = SwaggerModule.createDocument(app, swaggerConfig);
  SwaggerModule.setup('api', app, document);
  await app.listen(3000);
}
bootstrap();

function getApiDescription() {
  return 'The Ripple Science API';
}
