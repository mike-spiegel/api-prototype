import { AutoMap } from '@automapper/classes';
import { IsDate } from 'class-validator';
import { Column, Entity, ObjectIdColumn } from 'typeorm';

@Entity()
export class Participants {
  @ObjectIdColumn()
  _id: string;

  @Column()
  @AutoMap()
  readonly globalId: string;

  @Column()
  @AutoMap()
  readonly firstName: string;

  @Column()
  @AutoMap()
  readonly lastName: string;

  @Column()
  @AutoMap()
  readonly middleName: string;

  @Column()
  @AutoMap()
  readonly birthday: string;

  @Column()
  @AutoMap()
  readonly sex: string;

  @Column()
  @AutoMap()
  readonly race: string;

  @Column()
  @AutoMap()
  readonly ethnicity: string;

  @Column()
  @IsDate()
  readonly insertedAt: Date;

  @Column()
  @IsDate()
  @AutoMap()
  readonly modifiedAt: Date;
  //   readonly studies: Array<ParticipantStudyInfo>;
  //   readonly customVariables: Array<CustomVariableValues>;
  //   readonly contacts: Array<ParticipantContact>;
}
