import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';

@Module({
  imports: [
    TypeOrmModule.forRoot({
      type: 'mongodb',
      host: 'localhost',
      port: 27017,
      database: 'ripple-dev',
      synchronize: true, // FIXME: remove from prod build, will mess with schema
      logging: false,
      autoLoadEntities: true,
      migrations: ['src/core/dataaccess/migrations/**/*.ts'],
    }),
  ],
})
export class DataaccessModule {}
