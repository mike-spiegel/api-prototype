import { Module } from '@nestjs/common';
import { DataaccessModule } from './dataaccess/dataaccess.module';
import { MappersModule } from './mappers/mappers.module';

@Module({
  imports: [DataaccessModule, MappersModule],
})
export class CoreModule {}
