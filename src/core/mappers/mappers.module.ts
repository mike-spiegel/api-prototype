import { AutomapperModule } from '@automapper/nestjs';
import { classes } from '@automapper/classes';
import { Module } from '@nestjs/common';
import { ParticipantProfile } from '../../api/rest/participants/mappers/ParticipantProfile';

@Module({
  imports: [
    AutomapperModule.forRoot({
      options: [{ name: 'mapper', pluginInitializer: classes }],
      singular: true,
    }),
  ],
  providers: [ParticipantProfile],
})
export class MappersModule {}
